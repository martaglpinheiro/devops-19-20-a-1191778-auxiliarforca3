# Class Assignment 2 Report

## Overview

**Topic of this assignment:** Build Tools with Gradle

**Start Date:** 26 of March

**End Date:** 9 of April

## Analysis, Design and Implementation

### Prerequisites

For this tutorial you're going to need:

- Git installed;
- Gradle installed;
- Your own private remote repository already created.

###  --- **Part 1** --- 

### Clone the Gradle Basic Demo repository

Navigate to the correct path in your computer where you have your local repository located.
```sh
$ cd /path/to/your/repo
```

Create a new folder and navigate to inside of it. For this example we're going to make a new "ca2" folder.
```sh
$ mkdir ca2
$ cd ca2
```

Clone the Gradle Basic Demo repository into a new "part1" folder.
```sh
$ git clone <gradle-basic-demo-url> part1
```

### First push to master
Add the existing files to the repository. This stages all changes in <directory> for the next commit. 
```sh
$ git add .
```

Commit the files. This saves your changes to the local repository.
```sh
$ git commit -m "your message"
```

Make a first push to the remote repository. That's your current stable version.
```sh
$ git push -u origin master
```

### Buid Gradle
Let's begin by executing the build task. This task builds a .jar file with the application.
```sh
$ gradlew build
```
### Run the server
To run the server you'll need to execute on the project's root directory:
```sh
$ java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>
```
### Run a client
Open a different command line, letting the previous still open, and run the runClient task, again on the project's root directory.
```sh
$ gradlew runClient
```
The above task assumes the chat server's IP is "localhost" and its port is the one used on the previous command (the chosen <server port>).
### Add a new task to execute the server.
For this you need to update build.gradle with:
```gradle
task runServer(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a server that supports a chat on localhost:59001 "

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatServerApp'

    args '59001'
}
```
Check if the task was created and is ready to be executed by running the command:
```sh
$ gradle tasks
```
This command displays all the available gradle tasks.

To run any of the created tasks just execute:
```sh
$ gradle <the-name-of-the-task>
```
For example, to run the task we just created:
```sh
$ gradle runServer
```
Repeat both this steps after creating the next couple of tasks as well.
### Add a simple unit test and update the gradle script so that it is able to execute said test
An exemple of a test could be:
```gradle
package basic_demo;
import org.junit.Test;
import static org.junit.Assert.*;
  
  public class AppTest {
  @Test public void testAppHasAGreeting() {
  App classUnderTest = new App();
  assertNotNull("app should have a greeting", classUnderTest.getGreeting());
  }
}
```
The unit tests require junit 4.12 to execute so you'll need to add that dependency to build.grade, as such:
```gradle
test {
    useJUnit()
    maxHeapSize = '1G'
}
```

### Add a task that makes a copy of 'src' into a folder
Create a new task that copies the 'src' contents into a given backup folder, as such:
```gradle
task backupToFolder(type: Copy) {
    from 'src'
    into 'backupToFolder'
}
```

###  Add a task that makes a copy of 'src' into a zip file
Create a new task that copies the 'src' contents into a given zip file, as such:
```gradle
task backupToZipFile(type: Zip) {
    archiveFileName = "archive.zip"
    destinationDirectory = file('backupToZipFile')
    from 'src'
}
```

### Commit and Push

After you verify that everything is up and running you need to add this changes to your repository and add an identifying tag, such as:

```sh
$ git add .
$ git commit -m "message"
$ git push -u origin master
$ git tag <tag-name>
$ git push origin <tag-name>
```

###  --- **Part 2** --- 
### Create a new folder
Make a new folder inside your repository directory to store this new part.
```sh
§ cd path/to/repository/directory
$ mkdir part2
```
### Create a new branch
Create a new branch. For example:
```sh
$ git branch tut-basic-gradle
```

Switch to that branch. 
```sh
$ git checkout tut-basic-gradle
```
## Start a new Spring Project with Gradle
Navigate to https://start.spring.io and
start a new gradle spring project with the following dependencies: 

- Rest Repositories;
- Thymeleaf; 
- JPA; 
- H2.

Extract the generated zip file inside the folder you just created <part2>. We now have an "empty" spring application that can be built using gradle.

## Update scr folder
Delete the src folder, we want to use the one from a previous assignment <ca1>.

From the basic folder copy:

- the src folder;
- webpack.config.js;
- package.json.´

Also, remember to delete the folder  src/main/resources/static/built/ since this folder should be generated from the javascrit by the tool webpack.

## Gradle bootRun
You can now experiment with the application by using:
```sh
$ gradle bootRun
```
## Add the frontend plugin
We will add the gradle plugin org.siouan.frontend to the project so that gradle is also able to manage the frontend.

First, add the next plugin to your gradle.build:

```sh
id "org.siouan.frontend" version "1.4.1"
```

Next, still inside gradle.build, add the following code that configures the previous plugin:
``` gradle
frontend {
	nodeVersion = "12.13.1"
	assembleScript = "run webpack"
}
```

Lastly, now inside package.json, update your scripts section with:
``` gradle
"scripts": {
"watch": "webpack --watch -d",
"webpack": "webpack"
},
```
This configures the execution of the webpack.

### Build Gradle
You can now execute the build gradle task, as such:
```sh
gradle build
```
The tasks related to the frontend are also
executed and the frontend code is generated.

### Gradle bootRun
You may now execute the application by using:
```sh
gradle bootRun
```
Now you should check your http://localhost:8080/ to confirm that it's no longer blank.

###  Add a task that makes a copy of the generated jar
Add a task to gradle to copy the generated jar to a folder named "dist" located a the project root folder level, as such:
```gradle
task copyJar(type: Copy) {
	group = "Devops"
	description = "Copies the generated jar to dist folder"

	from 'build/libs/'
	into 'dist'
}
```

###  Add a task that makes a copy of the generated jar
Add a task to gradle to delete all the files generated by webpack. This new task should be executed automatically by gradle before the task clean, as such:
```gradle
task deleteWebpackFiles(type: Delete) {
	group = "Devops"
	description = "Delete all the files generated by webpack"
	delete 'src/main/resources/static/built'
}

clean.dependsOn deleteWebpackFiles
```

### Commit and Push

After you verify that everything is up and running you need to add this changes to your repository and add an identifying tag, such as:

```sh
$ git add .
$ git commit -m "message"
$ git push -u origin master
$ git tag <tag-name>
$ git push origin <tag-name>
```